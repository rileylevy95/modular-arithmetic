(ns ^:figwheel-hooks modular.core
  (:refer-clojure :exclude [ + - * /])
  (:require
   [clojure.pprint :refer [cl-format]]
   [clojure.core.async :as async :refer [chan go go-loop <! >! put! alt!]]

   [webjunk.bulma :as bulma]
   [webjunk.generic-math :refer [+ - * / abs sqrt pow exp log conjugate sgn]]
   [webjunk.complex :as complex :refer [i cis complex ensure-complex real imag]]
   [webjunk.dual :as dual :refer [dual tangent]]
   [webjunk.svg :as svg]
   [webjunk.pseudotag :refer [let-dissoc deftagfn]]

   [goog.dom :as gdom]
   [reagent.core :as reagent :refer [atom with-let]]
   [reagent.dom :as rdom]))






(def tau (* 2 Math/PI))

(deftagfn tex attr
  ([s] (tex attr s {}))
  ([s opts]
   [:span (merge attr
                 {:dangerouslySetInnerHTML
                  {:__html
                   (.renderToString js/katex s (clj->js opts))}})]))

(defn interp [x y]
  (fn [t]
    (+ (* (- 1 t) x)
       (* t y))))

(defn fmt [& args] (apply cl-format nil args))

(defn fit3-dual [[s0 s1] f]
  (let [N  (interp s0 s1)
        sf (comp f N)
        [z0 d0] (sf (dual 0 1))
        [z3 d3] (sf (dual 1 1))
        z1 (+ z0 (/ d0 3))
        z2 (- z3 (/ d3 3))]
    [z0 z1 z2 z3]))
(deftagfn cubic attr [[x0 y0] [x1 y1] [x2 y2] [x3 y3] & children]
  [:path (assoc attr
                :d (fmt "M ~a,~a C ~a,~a ~a,~a ~a,~a" x0 y0 x1 y1 x2 y2 x3 y3))
   children])

(deftagfn cubic-slice attr [[s0 s1] f]
  (apply cubic attr
         (fit3-dual [s0 s1] f)))


(deftagfn cubic-slices attr [N [start end] f]
  [:g attr
   (for [i (range (inc N))]
     (cubic-slice [(/ i N) (/ (inc i) N)]
       (comp f (interp start end))))])

(deftagfn arc attr [z r start end]
  (for [i (range 5)
        :let [t0 (/ i 5)
              t1 (/ (inc i) 5)
              [θ0 θ1] (map (interp start end) [t0 t1])]]
    (cubic-slices (assoc attr :key i)
                  (Math/ceil (/ (- start end) (/ tau 4)))
      [θ0 θ1]
      #(+ z (* r (cis %))))))

(deftype Modular [num modulus])

(defn nan? [x] (not= x x))

(defmethod + [Modular Modular]
  [x y]
  (let [modulus (.-modulus x)]
    (assert (= modulus (.-modulus y)))
    (Modular. (mod (+ (.-num x) (.-num y)) modulus)
              modulus)))
(defmethod + [Modular js/Number] [x y]
  (+ x (Modular. y (.-modulus x))))
(defmethod - Modular [x]
  (Modular. (mod (- (.-num x)) (.-modulus x)) x))


(defmethod * [Modular Modular]
  [x y]
  (let [modulus (.-modulus x)]
    (assert (= modulus (.-modulus y)))
    (Modular. (mod (* (.-num x) (.-num y)) modulus)
              modulus)))
(defmethod * [Modular js/Number] [x y]
  (* x (Modular. y (.-modulus x))))


(defn mod->root-of-unity [m]
  (let [x (.-num m)
        modulus (.-modulus m)]
    (* i (cis (* -1 x (/ tau modulus))))))

(deftagfn texat attr [z texstr]
  (let [[x y] (ensure-complex z)]
    (let-dissoc [{:keys [scale]} attr]
     [:g {:transform (fmt "translate(~a, ~a) scale(~a) scale(1,-1)" x y scale)}
      [svg/foreignObject {:z (complex -6 -10) :dz (complex 80 50)}
       [tex attr texstr]]])) )

(defn ->svg-coords [^js/SVGSVGElement svg x y]
  (let [p (.createSVGPoint svg)
        mat (-> svg .getScreenCTM .inverse)]
    (set! (.-x p) x)
    (set! (.-y p) y)
    (let [p (.matrixTransform p mat)]
      (complex (.-x p) (.-y p)))))

(defn select-event-to-chan [out-chan num event-name]
  (fn [_]
    (put! out-chan
          {:num num
           :event-name event-name})))

(defn selectors [event-chan modulus]
  (for [n (range modulus)
        :let [z (mod->root-of-unity (Modular. n modulus))]]
    (let [events #{:on-mouse-enter :on-mouse-out :on-click}]
      [svg/circle (apply merge
                         {:key n
                          :class "selector"}
                         (for [ev events]
                           [ev (select-event-to-chan event-chan n ev)]))
       z (min 0.25 (/ tau modulus 2.0))])))

(defn selector&clock [event-chan modulus hover]
  [:<>
   [svg/circle {:class :grid} 0 0.8]
   [svg/circle {:class :grid} 0 1.2]
   (for [n (range modulus)
         :let [z (mod->root-of-unity (Modular. n modulus))]]
     [texat
      {:key n
       :class (if (= n hover) :shadow)
       :scale (min (/ 0.2 modulus) 0.01)}
      z (str n)])
   (selectors event-chan modulus)])

(defn old-clock [state]
  (let [event-chan (chan)]
    (go-loop []
      (let [{:keys [num event-name]} (<! event-chan)]
        (case event-name
          :on-mouse-enter (swap! state assoc :hover num)
          :on-mouse-out   (swap! state dissoc :hover)
          :on-click       (swap! state update :clicked conj num)))
      (recur))
    (fn [state]
      [selector&clock event-chan (:modulus @state) (:hover @state)])))

(defn zip [& args] (apply map vector args))
(defn adjacents [col] (zip col (rest col)))

(deftagfn modlines attr [modulus nums]
  [:<>
   (for [[x y] (adjacents nums)]
     [svg/line (assoc attr :key x)
      (mod->root-of-unity x)
      (mod->root-of-unity y)])])

(defn title [name] [:h1.title.has-text-centered name])
(defn modulus-selector
  ([state] (modulus-selector state (constantly nil)))
  ([state click-hook]
   [:p.has-text-centered
    [tex {:class :is-size-4} "ℤ \\Big /"]
    [:input.input.is-narrow
     {:type :number
      :value (:modulus @state)
      :min 1
      :on-change
      (fn [e]
        (click-hook e)
        (swap! state assoc :clicked [])
        (swap! state assoc :modulus
               (let [parsed (-> e .-target .-value js/Number.parseInt)]
                 (if (nan? parsed) 0
                     (min 100 (max parsed 0))))))}]
    [tex {:class :is-size-4} "ℤ"]]))

(defn eqn-window [state op formatter]
  [:div.level.clamped.is-mobile
   [:span.overflow-auto
    (let [clicked (:clicked @state)
          modulus (:modulus @state)
          nums (if (empty? clicked) [1] clicked)]
      [tex (str (formatter nums)
                "\\equiv" (mod (reduce op nums) modulus)
                "\\pmod{" modulus "}")
       {:displayMode true}])]
   [:span.buttons
    [bulma/control {:left-icon-class "fa fa-undo"}
     [bulma/button
      {:on-click #(swap! state update :clicked pop)}]]
    [bulma/control {:left-icon-class "fa fa-trash"}
     [bulma/button
      {:on-click #(swap! state assoc :clicked [])}]]]])

(defn modular-addition []
  (with-let [state (atom {:clicked []
                          :modulus (+ 4 (rand-int 25))})]
    [:div.column.is-narrow
     [title "Modular addition"]
     [modulus-selector state]
     (let [modulus (:modulus @state)]
       (if (> modulus 0)
         [:<>
          [eqn-window state + #(apply str (interpose "+" %))]
          [:svg.diagram
           {:viewBox "-1.2 -1.2 2.4 2.4"
            :preserveAspectRatio "xMidYMid meet"}
           [modlines {:class :arrowed} modulus
            (reductions + (Modular. 0 modulus) (:clicked @state))]
           [old-clock state]]]))]))

(defn modular-multiplication []
  (with-let [state (atom {:clicked []
                          :modulus (+ 4 (rand-int 25))})]
    [:div.column.is-narrow
     [title "Modular multiplication"]
     [modulus-selector state]
     (let [modulus (:modulus @state)]
       (if (> modulus 0)
         [:<>
          [eqn-window state * (fn [nums] (apply str (map #(str "("%")") nums)))]
          [:svg.diagram
           {:viewBox "-1.2 -1.2 2.4 2.4"
            :preserveAspectRatio "xMidYMid meet"}
           [modlines {:class :arrowed} modulus
            (reductions * (Modular. 1 modulus) (:clicked @state))]
           [old-clock state]]]))]))

(defn take-unique [col]
  (if (empty? col) col
      (let [[x & xs] col]
        (cons x
         (take-while #(not= x %) xs)))))

(defn cosets []
  (let [state (atom {:clicked []
                     :modulus (+ 4 (rand-int 25))})
        event-chan (chan)
        random-generator! #(swap! state assoc :generator
                                  (inc (rand-int (dec (:modulus @state)))))]
    (random-generator!)
    (go-loop []
      (let [{:keys [event-name num]} (<! event-chan)]
        (case event-name
          :on-mouse-enter (swap! state assoc :hover num)
          :on-mouse-out   (swap! state dissoc :hover)
          nil))
      (recur))
    (fn []
      [:div.column.is-narrow
       [title "Cosets"]
       [modulus-selector state (fn [_] (random-generator!))]


       (let [modulus   (:modulus   @state)
             hover     (:hover     @state)
             generator (:generator @state)]
         [:<>
          [:p.has-text-centered
           "Generator: "
           [:input.input.is-narrow
            {:type :number
             :value (:generator @state)
             :min 1
             :on-change
             (fn [e]
               (swap! state assoc :generator
                      (let [parsed (-> e .-target .-value js/Number.parseInt)]
                        (if (nan? parsed) 0
                            (min (dec modulus) (max parsed 0))))))}]]

          (if (> modulus 0)
            [:svg.diagram
              {:viewBox "-1.2 -1.2 2.4 2.4"
               :preserveAspectRatio "xMidYMid meet"}
              (if hover
                (let [multiples (iterate #(mod (+ % generator) modulus)
                                         hover)
                      uniques (take-unique multiples)
                      coset  (concat uniques [(first uniques)])]
                  [modlines {} modulus (map #(Modular. % modulus) coset)]))
              [selector&clock event-chan modulus hover]])])])))



(defn hello-world []
  [:<>
   [:svg
    [:defs [:marker {:id "arrow"
                     :viewBox "-5 -5 10 10"
                     :orient "auto"
                     :markerWidth 10
                     :markerHeight 10
                     :refX 4
                     :refY 0}
            [:path {:d "M0,0 L-2,3 L5,0 L-2,-3 Z"}]]]]
   [:main.columns.is-centered.is-multiline.is-variable.is-7
    [modular-addition]
    [modular-multiplication]
    [cosets]
    ]])


(defn get-app-element []
  (gdom/getElement "app"))

(defn mount [el]
  (rdom/render [hello-world] el))

(defn mount-app-element []
  (when-let [el (get-app-element)]
    (mount el)))

;; conditionally start your application based on the presence of an "app" element
;; this is particularly helpful for testing this ns without launching the app
(mount-app-element)

;; specify reload hook with ^;after-load metadata
(defn ^:after-load on-reload []
  (mount-app-element)
  ;; optionally touch your app-state to force rerendering depending on
  ;; your application
  ;; (swap! app-state update-in [:__figwheel_counter] inc)
)
