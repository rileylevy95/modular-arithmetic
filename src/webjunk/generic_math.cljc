(ns webjunk.generic-math
  (:refer-clojure :exclude [+ - * / inc dec])
  #?(:cljs
     (:require-macros [webjunk.generic-math
                       :refer [defmacro- defmathfn-1 defmathfn-2]])))

#?(:cljs (def Number js/Number))

(defn illegal-argument [msg]
  (ex-info msg {}))

;; Support code for generic interfaces

;; by Konrad Hinsen

;; Copyright (c) Konrad Hinsen, 2009-2011. All rights reserved.  The use
;; and distribution terms for this software are covered by the Eclipse
;; Public License 1.0 (http://opensource.org/licenses/eclipse-1.0.php)
;; which can be found in the file epl-v10.html at the root of this
;; distribution.  By using this software in any fashion, you are
;; agreeing to be bound by the terms of this license.  You must not
;; remove this notice, or any other, from this software.

;;
;; A dispatch function that separates nulary, unary, binary, and
;; higher arity calls and also selects on type for unary and binary
;; calls.
;;
(defn nary-dispatch
  ([] ::nulary)
  ([x] (type x))
  ([x y]
   [(type x) (type y)])
  ([x y & more] ::nary))

;;
;; We can't use [::binary :default], so we need to define a root type
;; of the type hierarcy. The derivation for Object covers all classes,
;; but all non-class types will need an explicit derive clause.
;; Since non-class types are rare since Clojure 1.2, this need is
;; not likely to be frequent.
;;
(def root-type ::any)
#?(:clj (derive Object root-type))
#?(:cljs (derive js/Number root-type))
#?(:cljs (assert (isa? js/Number root-type)))




;;
;; Symbols referring to ::nulary and ::n-ary
;;
(def nulary-type ::nulary)
(def nary-type ::nary)

;; Universal zero and one values
;;
(defrecord zero-type [])
(derive zero-type root-type)
(def zero (new zero-type))

(defrecord one-type [])
(derive one-type root-type)
(def one (new one-type))

(defmulti +
  "Return the sum of all arguments. The minimal implementation for type
   ::my-type is the binary form with dispatch value [::my-type ::my-type]."
  {:arglists '([x] [x y] [x y & more])}
  nary-dispatch)

(defmethod + nulary-type
  []
  zero)

(defmethod + root-type
  [x] x)

(defmethod + [root-type zero-type]
  [x y] x)

(defmethod + [zero-type root-type]
  [x y] y)

(defmethod + nary-type
  [x y & more]
  (if more
    (recur (+ x y) (first more) (next more))
    (+ x y)))

(defmulti -
  "Return the difference of the first argument and the sum of all other
   arguments. The minimal implementation for type ::my-type is the binary
   form with dispatch value [::my-type ::my-type]."
  {:arglists '([x] [x y] [x y & more])}
  nary-dispatch)

(defmethod - nulary-type
  []
  (throw (illegal-argument
          "Wrong number of arguments passed")))

(defmethod - [root-type zero-type]
  [x y] x)

(defmethod - [zero-type root-type]
  [x y] (- y))

(defmethod - [root-type root-type]
  [x y] (+ x (- y)))

(defmethod - nary-type
  [x y & more]
  (if more
    (recur (- x y) (first more) (next more))
    (- x y)))

(defmulti *
  "Return the product of all arguments. The minimal implementation for type
   ::my-type is the binary form with dispatch value [::my-type ::my-type]."
  {:arglists '([x] [x y] [x y & more])}
  nary-dispatch)

(defmethod * nulary-type
  []
  one)

(defmethod * root-type
  [x] x)

(defmethod * [root-type one-type]
  [x y] x)

(defmethod * [one-type root-type]
  [x y] y)

(defmethod * nary-type
  [x y & more]
  (if more
    (recur (* x y) (first more) (next more))
    (* x y)))

;;;  i think it had issues defmulti / in cljs
(defmulti div
  "Return the quotient of the first argument and the product of all other
   arguments. The minimal implementation for type ::my-type is the binary
   form with dispatch value [::my-type ::my-type]."
  {:arglists '([x] [x y] [x y & more])}
  nary-dispatch)

(def / div)

(defmethod div nulary-type
  []
  (throw (illegal-argument
          "Wrong number of arguments passed")))

(defmethod div [root-type one-type]
  [x y] x)

(defmethod div [one-type root-type]
  [x y] (/ y))

(defmethod div [root-type root-type]
  [x y] (* x (/ y)))

(defmethod div nary-type
  [x y & more]
  (if more
    (recur (/ x y) (first more) (next more))
    (/ x y)))

(defmethod + [Number Number]
  [x y] (clojure.core/+ x y))

(defmethod - Number
  [x] (clojure.core/- x))

(defmethod * [Number Number]
  [x y] (clojure.core/* x y))

(defmethod div Number
  [x] (clojure.core// x))


;; This used to be in clojure.contrib.def (by Steve Gilardi),
;; which has not been migrated to the new contrib collection.
(defmacro defmacro-
  "Same as defmacro but yields a private definition"
  [name & decls]
  (list* `defmacro (with-meta name (assoc (meta name) :private true)) decls))

(defmacro- defmathfn-1
  [name]
  (let [math-symbol (symbol "Math" (str name))]
    `(do
       (defmulti ~name
         ~(str "Return the " name " of x.")
         {:arglists '([~'x])}
         type)
       (defmethod ~name ~'Number
         [~'x]
         (~math-symbol ~'x)))))

(defn- two-types [x y] [(type x) (type y)])

(defmacro- defmathfn-2
  [name]
  (let [math-symbol (symbol "Math" (str name))]
    `(do
       (defmulti ~name
         ~(str "Return the " name " of x and y.")
         {:arglists '([~'x ~'y])}
         two-types)
       (defmethod ~name [~'Number ~'Number]
         [~'x ~'y]
         (~math-symbol ~'x ~'y)))))

(defmathfn-1 acos)
(defmathfn-1 asin)
(defmathfn-1 atan)
(defmathfn-2 atan2)
(defmathfn-1 ceil)
(defmathfn-1 cos)
(defmathfn-1 exp)
(defmathfn-1 floor)
(defmathfn-1 log)
(defmathfn-2 pow)
(defmathfn-1 rint)
(defmathfn-1 sin)
(defmathfn-1 sqrt)
(defmathfn-1 tan)

(defmulti abs
  "Return the absolute value of x. If x is a BigDecimal, abs takes an optional
  math-context argument."
  {:arglists '([x] [x math-context])}
  (fn [x & more] (type x)))

(defmethod abs :default
  [x]
  (cond (neg? x) (- x)
        :else x))

(defmethod abs Number
  [x]
  (Math/abs x))

#?(:clj
   (do
     (defmethod abs java.math.BigDecimal
       ([x]
        (.abs x))
       ([x math-context]
        (.abs x math-context)))

     (defmethod abs java.math.BigInteger
       [x]
       (.abs x))

     (defmethod abs clojure.lang.BigInt
       [x]
       (if (nil? (.bipart x))
         (clojure.lang.BigInt/fromLong (abs (.lpart x)))
         (clojure.lang.BigInt/fromBigInteger (abs (.bipart x)))))

     (defmethod abs clojure.lang.Ratio
       [x]
       (/ (abs (numerator x))
          (abs (denominator x))))))

(defmulti round
  "Round x.
  If x is a BigDecimal, a math-context argument is also required:
    (round x math-context)
  If x is a Ratio,
    (round x) converts x to a double and rounds;
    (round x math-context) converts x to a BigDecimal and rounds."
  {:arglists '([x] [x math-context])}
  (fn [x & more] (type x)))

#?(:clj
   (do
     (doseq [c [java.lang.Float
                java.lang.Double]]
       (defmethod round c [x] (Math/round x)))

     (doseq [c [java.lang.Byte
                java.lang.Short
                java.lang.Integer
                java.lang.Long
                java.math.BigInteger
                clojure.lang.BigInt]]
       (defmethod round c [x] x))

     (defmethod round java.math.BigDecimal
       [x math-context]
       (.round x math-context))

     (defmethod round clojure.lang.Ratio
       ([x]
        (round (double x)))
       ([x math-context]
        (round (bigdec x) math-context))))
   :cljs (defmethod round Number [x]
           (Math/round x)))


(defmulti sgn
  "Return the sign of x (-1, 0, or 1)."
  {:arglists '([x])}
  type)

(defmethod sgn :default
  [x]
  (cond (zero? x) 0
        (> x 0) 1
        :else -1))

(defmulti conjugate
  "Return the conjugate of x."
  {:arglists '([x])}
  type)

(defmethod conjugate :default
  [x] x)

(defmulti sqr
  "Return the square of x."
  {:arglists '([x])}
  type)

(defmethod sqr :default
  [x]
  (* x x))

(defn approx=
  "Return true if the absolute value of the difference between x and y
   is less than eps."
  [x y eps]
  (< (abs (- x y)) eps))



(defmulti sinh  type)
(defmulti cosh  type)
(defmulti tanh  type)
(defmulti asinh type)
(defmulti acosh type)
(defmulti atanh type)

(defmethod sinh root-type [z]
  (/ (- (exp z) (exp (- z)))
     2))
(defmethod cosh root-type [z]
  (/ (+ (exp z) (exp (- z)))
     2))
(defmethod tanh root-type [z]
  (/ (- (exp z) (exp (- z)))
     (+ (exp z) (exp (- z)))))
(defmethod asinh root-type [z]
  (log (+ z (sqrt (+ 1 (sqr z))))))
(defmethod asinh root-type [z]
  (log (+ z (sqrt (- 1 (sqr z))))))
(defmethod atanh root-type [z]
  (/ (log (/ (+ 1 z) (- 1 z)))
     2))

(defn pow-by-exp [z u]
  (exp (* u (log z))))
(defmethod pow [root-type root-type] [z u]
  (pow-by-exp z u))

(defn powi-by-square-and-multiply [z n]
  (if (neg? n) (/ (pow z (- n)))
      (loop [n n
             powof2 z
             acc 1]
        (if (pos? n)
          (recur (bit-shift-right n 1)
                 (sqr powof2)
                 (case (bit-and 1 n)
                   1 (* acc powof2)
                   0 acc))
          acc))))

#?(:clj (do
          (defmethod pow [root-type Long] [z ^long n]
            (powi-by-square-and-multiply z n))
          (prefer-method pow [Number Number] [root-type Long]))
   :cljs (defmethod pow [root-type Number] [z n]
           (if (integer? n)
             (powi-by-square-and-multiply z n)
             (pow-by-exp z n))))

(defmethod sqrt root-type [z] (pow z (/ 1 2)))

(defn inc [x] (+ x 1))
(defn dec [x] (- x 1))
